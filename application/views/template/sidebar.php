<div class="main-sidebar">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand mt-2 mb-2">
			<img src="<?= base_url('assets/') ?>image/icon.png" alt="logo" width="50">
			<a href="<?= site_url('home') ?>">SITOYA</a>
		</div>
		<div class="sidebar-brand sidebar-brand-sm">
			<a href="<?= site_url('home') ?>">STY</a>
		</div>
		<ul class="sidebar-menu">
			<li class="menu-header">Menu</li>
			<li class="<?= ($this->uri->segment(1) == 'home' ? 'active' : '') ?>"><a class="nav-link" href="<?= site_url('home') ?>"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>
			<?php if ($this->session->userdata('role') == 'admin') : ?>
				<li class="<?= ($this->uri->segment(1) == 'user' ? 'active' : '') ?>"><a class="nav-link" href="<?= site_url('user') ?>"><i class="fas fa-users"></i> <span>Pengguna</span></a></li>
				<li class="nav-item dropdown <?= ($this->uri->segment(1) == 'C_fc' || $this->uri->segment(1) == 'C_riwayat' ? 'active' : '') ?>">
					<a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cash-register"></i> <span>Penjualan</span></a>
					<ul class="dropdown-menu">
						<li class="<?= ($this->uri->segment(1) == 'C_fc' ? 'active' : '') ?>"><a class="nav-link " href="<?= base_url('C_fc/index') ?>"><i class="fas fa-copy"></i>Transaksi</a></li>
						<li class="<?= ($this->uri->segment(1) == 'C_riwayat' ? 'active' : '') ?>"><a class="nav-link " href="<?= base_url('C_riwayat/index') ?>"><i class="fas fa-book"></i>Riwayat Transaksi</a></li>
					</ul>
				</li>
				<li class="<?= ($this->uri->segment(1) == 'C_belanja' ? 'active' : '') ?>"><a class="nav-link" href="<?= base_url('C_belanja/index') ?>"><i class="fas fa-cart-arrow-down"></i> <span>Belanja Produk</span></a></li>
				<li class="<?= ($this->uri->segment(1) == 'C_pengeluaran' ? 'active' : '') ?>"><a class="nav-link" href="<?= base_url('C_pengeluaran/index') ?>"><i class="fas fa-money-bill-wave"></i> <span>Pengeluaran</span></a></li>
				<li class="<?= ($this->uri->segment(1) == 'C_produk' ? 'active' : '') ?>"><a class="nav-link" href="<?= base_url('C_produk/index') ?>"><i class="fas fa-archive"></i> <span>Produk</span></a></li>
				<li class="<?= ($this->uri->segment(1) == 'C_laporan' ? 'active' : '') ?>"><a class="nav-link" href="<?= base_url('C_laporan/index') ?>"><i class="fas fa-wallet"></i> <span>Laporan Keuangan</span></a></li>
			<?php endif; ?>

			<?php if ($this->session->userdata('role') == 'pemilik') : ?>
				<li class="nav-item dropdown <?= ($this->uri->segment(1) == 'C_fc' || $this->uri->segment(1) == 'C_riwayat' ? 'active' : '') ?>">
					<a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cash-register"></i> <span>Penjualan</span></a>
					<ul class="dropdown-menu">
						<li class="<?= ($this->uri->segment(1) == 'C_riwayat' ? 'active' : '') ?>"><a class="nav-link " href="<?= base_url('C_riwayat/index') ?>"><i class="fas fa-book"></i>Riwayat Transaksi</a></li>
					</ul>
				</li>
				<li class="<?= ($this->uri->segment(1) == 'C_belanja' ? 'active' : '') ?>"><a class="nav-link" href="<?= base_url('C_belanja/index') ?>"><i class="fas fa-cart-arrow-down"></i> <span>Belanja Produk</span></a></li>
				<li class="<?= ($this->uri->segment(1) == 'C_pengeluaran' ? 'active' : '') ?>"><a class="nav-link" href="<?= base_url('C_pengeluaran/index') ?>"><i class="fas fa-money-bill-wave"></i> <span>Pengeluaran</span></a></li>
				<li class="<?= ($this->uri->segment(1) == 'C_laporan' ? 'active' : '') ?>"><a class="nav-link" href="<?= base_url('C_laporan/index') ?>"><i class="fas fa-wallet"></i> <span>Laporan Keuangan</span></a></li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'kasir') : ?>
				<li class="nav-item dropdown <?= ($this->uri->segment(1) == 'C_fc' || $this->uri->segment(1) == 'C_riwayat' ? 'active' : '') ?>">
					<a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cash-register"></i> <span>Penjualan</span></a>
					<ul class="dropdown-menu">
						<li class="<?= ($this->uri->segment(1) == 'C_fc' ? 'active' : '') ?>"><a class="nav-link " href="<?= base_url('C_fc/index') ?>"><i class="fas fa-copy"></i>Transaksi</a></li>
						<li class="<?= ($this->uri->segment(1) == 'C_riwayat' ? 'active' : '') ?>"><a class="nav-link " href="<?= base_url('C_riwayat/index') ?>"><i class="fas fa-book"></i>Riwayat Transaksi</a></li>
					</ul>
				</li>
			<?php endif ?>
		</ul>
	</aside>
</div>
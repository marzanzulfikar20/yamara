<section class="section">
	<div class="section-header">
		<h1>Belanja</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
			<div class="breadcrumb-item"><a href="#">Belanja</a></div>
			<div class="breadcrumb-item">Edit</div>
		</div>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4>Edit Belanja</h4>
					</div>
					<div class="card-body">
						<form method="POST" action="<?= site_url('c_belanja/update/'.$id) ?>">
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label for="nama_barang">Nama Produk</label>
										<input type="text" class="form-control" value="<?= set_value('nama_barang', $belanja->nama_barang) ?>" readonly>
										<span class="text-danger"><?= form_error('nama_barang'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="harga_beli">Harga Beli</label>
										<input type="number" name="harga_beli" id="harga_beli" class="form-control" placeholder="Isi Harga Beli" value="<?= set_value('harga_beli', $belanja->harga_beli) ?>">
										<span class="text-danger"><?= form_error('harga_beli'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="jumlah_barang">Jumlah Beli</label>
										<input type="jumlah_barang" name="jumlah_barang" id="jumlah_barang" class="form-control" placeholder="Isi Jumlah Beli" value="<?= set_value('jumlah_barang', $belanja->jumlah_barang) ?>">
										<span class="text-danger"><?= form_error('jumlah_barang'); ?></span>
									</div>
								</div>
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>
										<a href="<?= site_url('c_belanja/index') ?>" class="btn btn-danger mr-3 float-right"><i class="fas fa-undo-alt"></i> Kembali</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

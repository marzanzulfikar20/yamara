<table class="table table-striped">
	<tr>
		<th>Nama Barang</th>
		<td><?= $belanja->nama_barang ?></td>
	</tr>
	<tr>
		<th>Jumlah Barang</th>
		<td><?= $belanja->jumlah_barang ?></td>
	</tr>
	<tr>
		<th>Harga Beli</th>
		<td>Rp<?= number_format( $belanja->harga_beli,0) ?></td>
	</tr>
	<tr>
		<th>Nama Pengguna</th>
		<td><?= $belanja->nama?></td>
	</tr>
	<tr>
		<th>Tanggal Pembelian</th>
		<td><?= $belanja->tgl ?></td>
	</tr>
</table>

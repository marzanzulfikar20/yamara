<table class="table table-striped">
	<tr>
		<th>Jenis Pengeluaran</th>
		<td><?= $pengeluaran->jenis_pengeluaran ?></td>
	</tr>
	<tr>
		<th>Biaya Pengeluaran</th>
		<td>Rp<?= number_format($pengeluaran->biaya_pengeluaran,0) ?></td>
	</tr>
	<tr>
		<th>Nama Pengguna</th>
		<td><?= $pengeluaran->nama?></td>
	</tr>
</table>

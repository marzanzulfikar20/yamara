<section class="section">
	<div class="section-header">
		<h1>Pengeluaran</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
			<div class="breadcrumb-item"><a href="#">Pengeluaran</a></div>
			<div class="breadcrumb-item">Create</div>
		</div>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4>Tambah Pengeluaran</h4>
					</div>
					<div class="card-body">
						<form method="POST" action="<?= site_url('c_pengeluaran/create') ?>">
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label for="jenis_pengeluaran">Jenis Pengeluaran</label>
										<input type="text" name="jenis_pengeluaran" id="jenis_pengeluaran" class="form-control" placeholder="Isi Jenis Pengeluaran beserta Keterangannya" value="<?= set_value('jenis_pengeluaran') ?>">
										<span class="text-danger"><?= form_error('jenis_pengeluaran'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="biaya_pengeluaran">Biaya Pengeluaran</label>
										<input type="number" name="biaya_pengeluaran" id="biaya_pengeluaran" class="form-control" placeholder="Isi Biaya Pengeluaran" value="<?= set_value('biaya_pengeluaran') ?>">
										<span class="text-danger"><?= form_error('biaya_pengeluaran'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="tgl">Tanggal</label>
										<input type="date" name="tgl" id="tgl" min="<?=date('Y-m-d') ?>" class="form-control" placeholder="Isi Tanggal" value="<?= set_value('tgl') ?>">
										<span class="text-danger"><?= form_error('tgl'); ?></span>
									</div>
								</div>
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>
										<a href="<?= site_url('c_pengeluaran/index') ?>" class="btn btn-danger mr-3 float-right"><i class="fas fa-undo-alt"></i> Kembali</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

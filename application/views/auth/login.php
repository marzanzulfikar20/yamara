<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>Login | SITOYA</title>

	<link rel="icon" href="<?= base_url('assets/') ?>image/icon.png" type="image/x-icon">

	<!-- General CSS Files -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<!-- Template CSS -->
	<link rel="stylesheet" href="<?= base_url('assets/') ?>css/style.css">
	<link rel="stylesheet" href="<?= base_url('assets/') ?>css/components.css">
</head>

<body>
	<div id="app">
		<section class="section">
			<div class="container mt-5">
				<div class="row">
					<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
						<div class="login-brand">
							<img src="<?= base_url('assets/') ?>image/icon.png" alt="logo" width="100">
							<p>SITOYA</p>
						</div>

						<div class="card card-primary">
							<div class="card-header">
								<h4>Login</h4>
							</div>

							<div class="card-body">
								<form method="POST" action="<?= site_url('auth') ?>" class="needs-validation" novalidate="">
									<div class="form-group">
										<label for="username" style="font-size: 12pt;">Username</label>
										<input id="username" type="text" class="form-control" name="username" tabindex="1" required autofocus>
										<span class="text-danger"><?= form_error('username'); ?></span>
									</div>

									<div class="form-group">
										<div class="d-block">
											<label for="password" class="control-label" style="font-size: 12pt;">Password</label>
										</div>
										<input id="password" type="password" class="form-control" name="password" tabindex="2" required>
										<span class="text-danger"><?= form_error('password'); ?></span>
									</div>

									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
											Login
										</button>
									</div>
								</form>

							</div>
						</div>
						<div class="simple-footer">
							Copyright &copy; Stisla 2018
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- General JS Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script src="<?= base_url('assets/') ?>js/stisla.js"></script>

	<!-- JS Libraies -->

	<!-- Template JS File -->
	<script src="<?= base_url('assets/') ?>js/scripts.js"></script>
	<script src="<?= base_url('assets/') ?>js/custom.js"></script>

	<!-- Sweetalert2 -->
	<script src="<?= base_url('assets/') ?>js/sweetalert2.all.js"></script>

	<?php if ($this->session->flashdata('logout')) : ?>
		<script>
			$(document).ready(function() {
				Swal.fire({
					title: 'Sukses!',
					text: 'Sukses Logout',
					icon: 'success',
				});
			});
		</script>
	<?php endif; ?>

	<?php if ($this->session->flashdata('login_first')) : ?>
		<script>
			$(document).ready(function() {
				Swal.fire({
					title: 'Peringatan!',
					text: "<?= $this->session->flashdata('login_first') ?>",
					icon: 'error',
				});
			});
		</script>
	<?php endif; ?>

	<?php if ($this->session->flashdata('message_error')) : ?>
		<script>
			$(document).ready(function() {
				Swal.fire({
					title: 'Error!',
					text: "<?= $this->session->flashdata('message_error') ?>",
					icon: 'error',
				});
			});
		</script>
	<?php endif; ?>

	<!-- Page Specific JS File -->
</body>

</html>

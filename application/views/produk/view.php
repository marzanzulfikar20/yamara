<table class="table table-striped">
	<tr>
		<th>Nama Barang</th>
		<td><?= $produk->nama_barang ?></td>
	</tr>
	<tr>
		<th>Kode Barang</th>
		<td><?= $produk->kode_barang ?></td>
	</tr>
	<tr>
		<th>Harga Jual</th>
		<td>Rp<?= number_format($produk->harga_barang, 0) ?></td>
	</tr>
	<tr>
		<th>Stock</th>
		<td><?= $stock[0]->stok ?></td>
	</tr>
	<tr>
		<th>Gambar</th>
		<td><img src="<?= base_url("uploads/") . $produk->produk_img ?>" width="50%"></td>
	</tr>
</table>
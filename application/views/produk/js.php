<script>
	var tableContent;
	$.fn.dataTable.ext.errMode = 'none';

	$(document).ready(function() {
		tableContent = $('#produk_table').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: '<?php echo base_url('produk/datatable'); ?>',
				type: 'POST',
			},
			ordering: true,
			searching: true,
			pageLength: 10,
			responsive: true,
			language: {
				search: "<span>Cari Data:</span> _INPUT_",
				searchPlaceholder: 'Ketik Untuk Mencari...',
				loadingRecords: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>',
				processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>',
				lengthMenu: "Menampilkan _MENU_ records per halaman",
				zeroRecords: "Maaf data tidak ditemukan",
				info: "Menampilkan halaman _PAGE_ dari _PAGES_",
				infoEmpty: "Tidak ada data tersedia",
			},
			order: [],
			columns: [{
					data: "no",
					bSortable: false
				},
				{
					data: "nama_barang"
				},
				{
					data: "kode_barang"
				},
				{
					data: "harga_barang"
				},
				{
					data: "stok"
				},
				{
					mData: null,
					bSortable: false,
					mRender: function(data) {
						return '<button type="button" data-url="<?= site_url('C_produk/view/') ?>' + data.id_barang + '" class="btn btn-info btn-sm detail" data-toggle="modal" data-target="#detailModal"><i class="fas fa-eye"></i></button> <a href="<?php echo base_url('C_produk/update/'); ?>' + data.id_barang + '"><button type="button" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></button></a> <button type="button" data-id="' + data.id_barang + '" data-url="<?= site_url('C_produk/delete/') ?>' + data.id_barang + '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>';
					}
				},
			],
			columnDefs: [{
					targets: [3],
					render: function(data) {
						return 'Rp' + numberWithCommas(data)
					},
				},
				{
					targets: [4],
					render: function(data) {
						if (data == null) {
							return '0';
						} else {
							return data;
						}
					},
				},
			],
		});

		$('.select2-dropdown').select2({
			placeholder: "-- Pilih Data --",
			allowClear: true
		});

		$(document).on('click', '.detail', function() {

			$.ajax({
				url: $(this).data('url'),
				type: 'GET',
				beforeSend: function() {
					$('#detailModal .modal-body').html('<h4 class="text-center">Memuat . . .</h4>')
				},
				success: function(response) {
					if (response) {
						$('#detailModal .modal-body').html(response)
					}
				},
				error: function(data) {
					console.log(data)
				}
			})
		})

		$(document).on('click', '.delete', function() {
			var url = $(this).data('url')
			var user_id = $(this).data('id')
			var sess_id = '<?= $this->session->userdata('id') ?>';
			Swal.fire({
				title: 'Konfirmasi Hapus',
				text: "Anda yakin ingin hapus data ini ?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya',
				cancelButtonText: 'Batal',
			}).then(function(result) {
				if (result.value) {
					Swal.fire({
						title: 'Sukses!',
						text: 'Data Berhasil Dihapus!',
						icon: 'success',
						showConfirmButton: false,
						timer: 1500
					}).then(function(result) {
						$.ajax({
							url: url,
							type: 'DELETE',
							success: function(response) {
								$('#produk_table').DataTable().ajax.reload()
							},
						})
					})
				}
			})
		})

		function numberWithCommas(x) {
			return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		}
	});
</script>
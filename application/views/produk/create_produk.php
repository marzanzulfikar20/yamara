<section class="section">
	<div class="section-header">
		<h1>Produk</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
			<div class="breadcrumb-item"><a href="#">Produk</a></div>
			<div class="breadcrumb-item">Create</div>
		</div>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4>Tambah Produk</h4>
					</div>
					<div class="card-body">
						<form method="POST" enctype="multipart/form-data" action="<?= site_url('c_produk/create') ?>">
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="kategori_barang">Kategori</label>
										<select id="kategori_barang" name="id_kategori" class="form-control mb-3">
											<option value="0">Pilih Kategori</option>
											<?php foreach ($kategori as $key => $value) {
												echo '<option value="' . $value->id_kategori . '">' . $value->nama_kategori . '</option>';
											} ?>
										</select>
										<span class="text-danger"><?= form_error('id_kategori'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="nama_barang">Nama Produk</label>
										<input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Isi Nama Produk" value="<?= set_value('nama_barang') ?>">
										<span class="text-danger"><?= form_error('nama_barang'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="kode_barang">Kode Produk</label>
										<input type="number" name="kode_barang" id="kode_barang" class="form-control" placeholder="Isi Kode Produk" value="<?= set_value('kode_barang') ?>">
										<span class="text-danger"><?= form_error('kode_barang'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="harga_barang">Harga Jual</label>
										<input type="harga_barang" name="harga_barang" id="harga_barang" class="form-control" placeholder="Isi Harga" value="<?= set_value('harga_barang') ?>">
										<span class="text-danger"><?= form_error('harga_barang'); ?></span>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label for="produk_img">Gambar</label>
										<input type="file" name="produk_img" id="produk_img" class="form-control" placeholder="Isi Gambar Produk" value="<?= set_value('produk_img') ?>">
										<span class="text-danger"><?= form_error('produk_img'); ?></span>
									</div>
								</div>

								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>
										<a href="<?= site_url('c_produk/index') ?>" class="btn btn-danger mr-3 float-right"><i class="fas fa-undo-alt"></i> Kembali</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
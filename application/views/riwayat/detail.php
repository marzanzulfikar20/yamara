<section class="section">
    <div class="section-header">
        <h1>Penjualan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Penjualan</a></div>
            <div class="breadcrumb-item"><a href="#">Transaksi</a></div>
            <div class="breadcrumb-item">Invoice</div>
        </div>
    </div>
    <div class="section-body">
        <div class="invoice">
            <div class="invoice-print">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="invoice-title">
                            <h2>Invoice</h2>
                            <div class="invoice-number">Order #<?php echo $id_transaksi ?></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <address>
                                    <strong>TOKO YAMARA</strong><br>
                                </address>
                            </div>
                            <div class="col-md-6">
                                <address>
                                    <strong>Kasir</strong><br>
                                    <?php echo $transaksi->nama; ?>

                                </address>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Order Date:</strong><br>
                                    <?php echo date('d-m-Y') ?><br><br>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="section-title">Rincian Transaksi</div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered table-md">
                                <tr>
                                    <th data-width="40">#</th>
                                    <th>Produk</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Jumlah Barang</th>
                                    <th class="text-right">Totals</th>
                                </tr>
                                <?php
                                $total = 0;
                                foreach ($invoice as $k => $v) : ?>
                                    <tr>
                                        <th><?= ++$k  ?></th>
                                        <th><?= $v->nama_barang ?></th>
                                        <th class="text-center"><?= $v->harga_barang ?></th>
                                        <th class="text-center"><?= $v->jumlah_barang_transaksi ?></th>
                                        <th class="text-right">Rp<?= number_format($total += $v->harga_barang * $v->jumlah_barang_transaksi, 2, ',', '.') ?></th>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <div class="row mt-4">
                            <div class="col-lg-8">
                            </div>
                            <div class="col-lg-4 text-right">
                                <hr class="mt-2 mb-2">
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Total</div>
                                    <div class="invoice-detail-value invoice-detail-value-lg">Rp<?= number_format($total, 2, ',', '.') ?></div>
                                </div>
                                <div class="invoice-detail-item">
                                    <a href="<?php echo site_url('C_riwayat/index') ?>" class="btn btn-danger">Kembali</a>
                                    <a href="<?php echo site_url('C_fc/cetak/' . $id_transaksi) ?>" target="_blank" class="btn btn-primary">Cetak PDF</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</section>
<script>
    var tableContent;
    $.fn.dataTable.ext.errMode = 'none';

    $(document).ready(function() {
        tableContent = $('#riwayat_tabel').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '<?php echo base_url('C_riwayat/get_datatable'); ?>',
                type: 'POST',
            },
            ordering: true,
            searching: true,
            pageLength: 10,
            responsive: true,
            language: {
                search: "<span>Cari Data:</span> _INPUT_",
                searchPlaceholder: 'Ketik Untuk Mencari...',
                loadingRecords: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>',
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>',
                lengthMenu: "Menampilkan _MENU_ records per halaman",
                zeroRecords: "Maaf data tidak ditemukan",
                info: "Menampilkan halaman _PAGE_ dari _PAGES_",
                infoEmpty: "Tidak ada data tersedia",
            },
            order: [],
            columns: [{
                    data: "no",
                    bSortable: false
                },
                {
                    data: "id_transaksi"
                },
                {
                    data: "nama"
                },
                {
                    data: "tgl"
                },
                {
                    mData: null,
                    bSortable: false,
                    mRender: function(data) {
                        return '<a href="<?= site_url('C_riwayat/view/') ?>' + data.id_transaksi + '" class="btn btn-info" ><i class="fas fa-eye"></i></a>';
                    }
                },
            ],
        });

        $('.select2-dropdown').select2({
            placeholder: "-- Pilih Data --",
            allowClear: true
        });

    });
</script>
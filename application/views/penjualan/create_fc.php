<section class="section">
	<div class="section-header">
		<h1>Penjualan</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
			<div class="breadcrumb-item"><a href="#">Transaksi</a></div>
			<div class="breadcrumb-item">Create</div>
		</div>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4>Tambah Penjualan</h4>
					</div>
					<div class="card-body">
						<!-- <h2>Shopping Cart Dengan Ajax dan Codeigniter</h2> -->
						<!-- <hr /> -->
						<div class="row">
							<div class="col-md-6">
								<h4>Produk</h4>
								<select id="kategori_barang" class="form-control mb-3">
									<option value="0">Pilih Kategori</option>
									<?php foreach ($kategori as $key => $value) {
										echo '<option value="' . $value->id_kategori . '">' . $value->nama_kategori . '</option>';
									} ?>
								</select>
								<input type="text" id="search_barang" class="form-control" placeholder="Masukkan Nama Produk" disabled>
								<div class="row" id="produk"></div>

							</div>
							<div class="col-md-6">
								<h4>Shopping Cart</h4>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Produk</th>
											<th>Harga</th>
											<th>Qty</th>
											<th>Subtotal</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody id="detail_cart">
									</tbody>
								</table>
								<button class="btn btn-success float-right checkout"><i class="fas fa-shopping-cart"></i> Checkout</button>
								<button class="btn btn-danger" id="clear_cart"><i class="fas fa-trash"></i> Clear</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
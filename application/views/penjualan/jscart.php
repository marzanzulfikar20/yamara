<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.add_cart', function() {
            var produk_id = $(this).data("produkid");
            var produk_nama = $(this).data("produknama");
            var produk_harga = $(this).data("produkharga");
            var stok = $(this).data("stok");
            var quantity = $('#' + produk_id).val();

            if (stok < quantity) {
                Swal.fire({
                    title: 'Melebihi Jumlah Stok',
                    text: "Melebihi Jumlah Stok",
                    icon: 'error',
                })
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>C_cart/add_to_cart",
                    method: "POST",
                    data: {
                        produk_id: produk_id,
                        produk_nama: produk_nama,
                        produk_harga: produk_harga,
                        quantity: quantity
                    },
                    success: function(data) {
                        if (data == 'lebih_stok') {
                            Swal.fire({
                                title: 'Melebihi Jumlah Stok',
                                text: "Melebihi Jumlah Stok",
                                icon: 'error',
                            })
                        } else {
                            $('#detail_cart').html(data);
                        }
                    }
                });
            }

        });

        // Load shopping cart
        $('#detail_cart').load("<?php echo base_url(); ?>C_cart/load_cart");

        //Hapus Item Cart
        $(document).on('click', '.hapus_cart', function() {
            var row_id = $(this).attr("id"); //mengambil row_id dari artibut id
            $.ajax({
                url: "<?php echo base_url(); ?>C_cart/hapus_cart",
                method: "POST",
                data: {
                    row_id: row_id
                },
                success: function(data) {
                    $('#detail_cart').html(data);
                }
            });
        });

        $(document).on('click', '#clear_cart', function() {
            var row_id = $(this).attr("id"); //mengambil row_id dari artibut id
            $.ajax({
                url: "<?php echo base_url(); ?>C_cart/clear_cart",
                method: "POST",
                success: function(data) {
                    $('#detail_cart').html(data);
                }
            });
        });
        $(document).on('keyup', '#bayar', function() {
            var bayar = $(this).val();
            var total_bayar = $('#total_bayar').text();
            var kembalian = bayar - parseInt(total_bayar.replace(',', ''));
            $('#kembalian').val(kembalian);
            // alert(kembalian);
        });
        $(document).on('click', '.checkout', function() {
            var bayar = $('#bayar').val()
            var kembalian = $('#kembalian').val()
            Swal.fire({
                title: 'Konfirmasi Checkout',
                text: "Pastikan Barang Sudah Benar",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal',
            }).then(function(result) {
                if (result.isConfirmed) {
                    if (bayar == '' || kembalian < 0) {
                        Swal.fire({
                            title: 'Tidak Boleh Kosong Atau Minus',
                            text: "Kolom Bayar Harus Diisi!",
                            icon: 'error',
                        })
                    } else {
                        window.location.href = "<?php echo site_url('c_fc/checkout') ?>"
                    }
                }
            })
        });

        $(document).on('change', '#kategori_barang', function() {
            var kategori = $(this).val();
            if (kategori == '' || kategori == '0') {
                $('#search_barang').attr('disabled', 'disabled');
            } else {
                $('#search_barang').removeAttr('disabled');
            }
        });

        $("#search_barang").keyup(function() {
            var barang = $(this).val();
            var id_kategori = $('#kategori_barang').val();
            $.ajax({
                url: "<?php echo base_url(); ?>C_cart/search_barang",
                method: "POST",
                data: {
                    barang: barang,
                    id_kategori: id_kategori
                },
                success: function(data) {
                    $('#produk').html(data);
                }
            });
        });
    });
</script>
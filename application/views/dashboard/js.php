<script>
	var grafikPerbulan = <?= $grafik_perbulan ?>;
	var labelPerbulan = [];
	var dataPerbulan = [];
	var colorPerbulan = [];
	for (var i in grafikPerbulan) {
		labelPerbulan.push(grafikPerbulan[i].label);
		dataPerbulan.push(grafikPerbulan[i].data);
		colorPerbulan.push(grafikPerbulan[i].color);
	}

	var ctx = document.getElementById('myChartPerbulan');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labelPerbulan,
			datasets: [{
				label: 'Pendapatan',
				data: dataPerbulan,
				backgroundColor: colorPerbulan,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				y: {
					beginAtZero: true
				}
			}
		}
	});
</script>

<?php if ($this->session->flashdata('message_warning')) : ?>
	<script>
		$(document).ready(function() {
			Swal.fire({
				title: 'Info!',
				text: "<?= $this->session->flashdata('message_warning') ?>",
				icon: 'warning',
			});
		});
	</script>
<?php endif; ?>

<?php if ($this->session->flashdata('login_sukses')) : ?>
	<script>
		$(document).ready(function() {
			Swal.fire({
				title: 'Sukses!',
				text: "<?= $this->session->flashdata('login_sukses') ?>",
				icon: 'success',
			});
		});
	</script>
<?php endif; ?>
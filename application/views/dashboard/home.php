<section class="section">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-12">
			<div class="card card-statistic-2">
				<div class="card-stats">
					<div class="card-stats-title">
						User
						<?php if ($this->session->userdata('role') == 'admin') : ?>
							<a href="<?= site_url('user') ?>" class="btn btn-info float-right rounded-pill">Selengkapnya <i class="fas fa-chevron-right"></i></a>
						<?php endif ?>
					</div>
				</div>
				<div class="card-icon shadow-primary bg-primary">
					<i class="fas fa-users"></i>
				</div>
				<div class="card-wrap">
					<div class="card-header">
						<h4>Jumlah Users</h4>
					</div>
					<div class="card-body">
						<?= $total_user ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12">
			<div class="card card-statistic-2">
				<div class="card-stats">
					<div class="card-stats-title">
						User Aktif
						<?php if ($this->session->userdata('role') == 'admin') : ?>
							<a href="<?= site_url('user') ?>" class="btn btn-info float-right rounded-pill">Selengkapnya <i class="fas fa-chevron-right"></i></a>
						<?php endif ?>
					</div>
				</div>
				<div class="card-icon shadow-primary bg-primary">
					<i class="fas fa-user"></i>
				</div>
				<div class="card-wrap">
					<div class="card-header">
						<h4>Jumlah User Aktif</h4>
					</div>
					<div class="card-body">
						<?= $total ?>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4>Grafik Perbulan Tahun <?= date('Y') ?></h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="div" style="text-align: center">
								<canvas id="myChartPerbulan" width="200" height="200"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4>Total Penjualan Hari Ini</h4>
				</div>
				<div class="card-body">
					<h5>Rp.<?php echo number_format($penjualan['pendapatan'], 2, ',', '.') ?></h5>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h4>Barang Terlaris Bulan <?= date('F') ?></h4>
				</div>
				<div class="card-body">
					<table class="table table-bordered">
						<tr>
							<th>Nama Barang</th>
							<th>Jumlah Barang Terjual</th>
						</tr>
						<?php foreach ($barang_terlaris_perbulan as $value) : ?>
							<tr>
								<td><?= $value['nama_barang'] ?></td>
								<td><?= $value['sum'] ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h4>Barang Tidak Laku Bulan <?= date('F') ?></h4>
				</div>
				<div class="card-body">
					<table class="table table-bordered">
						<tr>
							<th>Nama Barang</th>
							<th>Jumlah Barang Terjual</th>
						</tr>
						<?php foreach ($barang_tidak_laku_perbulan as $value) : ?>
							<tr>
								<td><?= $value['nama_barang'] ?></td>
								<td><?= $value['sum'] == null ? '0' : $value['sum'] ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
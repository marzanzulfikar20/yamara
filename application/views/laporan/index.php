<div class="section-body">
	<div class="row">
		<div class="col-6 col-md-6 col-lg-6">
			<div class="card">
				<div class="card-header">
					<h4>Laporan Per Bulan</h4>
					<div class="card-header-action">
					</div>
				</div>
				<div class="card-body">
					<form method="POST" action="<?= site_url('C_laporan/laporan') ?>">
						<div class="row">
							<div class="col-12">
								<!-- <label for="kategori">Kategori</label>
								<select class="custom-select select2_dropdown" name="jenis_laporan" required>
									<option></option>
									<option value="1">Foto Copy</option>
									<option value="2">Alat Tulis Kantor</option>
								</select> -->
							</div>
							<div class="col-6">
								<div class="form-group">
									<label for="bulan">Bulan</label>
									<select name="bulan" class="custom-select select2_dropdown" id="bulan" required>
										<option></option>
										<option value="1">Januari</option>
										<option value="2">Februari</option>
										<option value="3">Maret</option>
										<option value="4">April</option>
										<option value="5">Mei</option>
										<option value="6">Juni</option>
										<option value="7">Juli</option>
										<option value="8">Agustus</option>
										<option value="9">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<div class="form-group">
										<label for="tahun">Tahun</label>
										<select name="tahun" class="custom-select select2_dropdown" id="tahun" required>
											<option></option>
											<?php
											$thn_skr = date('Y');
											for ($x = $thn_skr; $x >= 2010; $x--) {
											?>
												<option value="<?php echo $x ?>"><?php echo $x ?></option>
											<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<button type="submit" class="btn btn-rounded btn-sm btn-success waves-effect waves-light m-r-10">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-6 col-md-6 col-lg-6">
			<div class="card">
				<div class="card-header">
					<h4>Laporan Per Tahun</h4>
					<div class="card-header-action">
					</div>
				</div>
				<div class="card-body">
					<form method="POST" action="<?= site_url('C_laporan/laporan') ?>">
						<!-- <label for="kategori">Kategori</label>
						<select class="custom-select select2_dropdown" required>
							<option></option>
							<option value="1">Foto Copy</option>
							<option value="2">Alat Tulis Kantor</option>
						</select> -->
						<div class="form-group">
							<label for="inlineFormCustomSelect">Tahun</label>
							<div class="form-group">
								<div class="input-group">
									<select name="tahun" class="custom-select select2_dropdown" id="inlineFormCustomSelect" required>
										<option></option>
										<?php
										$thn_skr = date('Y');
										for ($x = $thn_skr; $x >= 2010; $x--) {
										?>
											<option value="<?php echo $x ?>"><?php echo $x ?></option>
										<?php
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-rounded btn-sm btn-success waves-effect waves-light m-r-10">Submit</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>
</section>

<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Detail Belanja</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
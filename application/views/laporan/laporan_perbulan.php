<div class="section-body">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12">
			<div class="card">
				<div class="card-header">
					<h4>Laporan Per Bulan <?= $bulan . ' ' . $tahun ?></h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="laporan_table">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Transaksi</th>
									<th>User</th>
									<th>Jenis Transaksi</th>
									<th>Total Harga</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($laporan as $i => $value) : ?>
									<tr>
										<td width="10px"><?= ++$i ?></td>
										<td width="100px"><?= $value->id_transaksi ?></td>
										<td><?= $value->nama ?></td>
										<td>
											<?php
											$jumlah = 0;
											$detail_transaksi = $model_transaksi->detail_transaksi_join($value->id_transaksi);
											foreach ($detail_transaksi as $val) :
												echo $val->nama_barang . '<br>';
												$jumlah += $val->harga_barang * $val->jumlah_barang_transaksi;
											endforeach ?>
										</td>
										<td>
											Rp.<?= number_format($jumlah) ?>,00
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<script>
    $(document).ready(function() {
        $('.select2_dropdown').select2({
            placeholder: "-- Pilih Data --",
            allowClear: true
        });
        $('#laporan_table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        });
    })
</script>
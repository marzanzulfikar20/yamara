<form method="POST" action="<?= site_url('user/update/' . $user->id) ?>">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="form-group">
				<label for="nama">Nama User</label>
				<input type="text" name="nama" id="nama" class="form-control" placeholder="Isi Nama" value="<?= set_value('nama', $user->nama) ?>">
				<span class="text-danger"><?= form_error('nama'); ?></span>
			</div>
		</div>
		<div class="col-sm-6 col-md-6">
			<div class="form-group">
				<label for="no_hp">No HP</label>
				<input type="number" name="no_hp" id="no_hp" class="form-control" placeholder="Isi No HP" value="<?= set_value('no_hp', $user->no_hp) ?>">
				<span class="text-danger"><?= form_error('no_hp'); ?></span>
			</div>
		</div>
		<div class="col-sm-6 col-md-6">
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" name="email" id="email" class="form-control" placeholder="Isi Email" value="<?= set_value('email', $user->email) ?>">
				<span class="text-danger"><?= form_error('email'); ?><?= $this->session->flashdata('error_email') ?></span>
			</div>
		</div>
		<div class="col-sm-6 col-md-6">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" name="username" id="username" class="form-control" placeholder="Isi Username" value="<?= set_value('username', $user->username) ?>">
				<span class="text-danger"><?= form_error('username'); ?><?= $this->session->flashdata('error_username') ?></span>
			</div>
		</div>
		<div class="col-sm-6 col-md-6">
			<div class="form-group">
				<label for="select2-role">Role</label>
				<select class="form-control select2-dropdown" name="role" id="select2-role">
					<option></option>
					<option value="admin" <?= (set_value('role', $user->role) == 'admin' ? 'selected' : '') ?>>Admin</option>
					<option value="pemilik" <?= (set_value('role', $user->role) == 'pemilik' ? 'selected' : '') ?>>Pemilik</option>
					<option value="kasir" <?= (set_value('role', $user->role) == 'kasir' ? 'selected' : '') ?>>Kasir</option>
				</select>
				<span class="text-danger"><?= form_error('role'); ?></span>
			</div>
		</div>
		<div class="col-sm-12 col-md-12">
			<div class="form-group">
				<label for="select2-active">Is Active</label>
				<select class="form-control select2-dropdown" name="is_active" id="select2-active">
					<option></option>
					<option value="1" <?= (set_value('is_active', $user->is_active) == 1 ? 'selected' : '') ?>>Aktif</option>
					<option value="0" <?= (set_value('is_active', $user->is_active) == 0 ? 'selected' : '') ?>>Tidak Aktif</option>
				</select>
				<span class="text-danger"><?= form_error('is_active'); ?></span>
			</div>
		</div>
		<div class="col-sm-12 col-md-12">
			<div class="form-group">
				<button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>
				<a href="<?= site_url('user') ?>" class="btn btn-danger mr-3 float-right"><i class="fas fa-undo-alt"></i> Kembali</a>
			</div>
		</div>
	</div>
</form>
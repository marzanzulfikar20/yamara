<script>
	var tableContent;
	$.fn.dataTable.ext.errMode = 'none';

	$(document).ready(function() {
		tableContent = $('#user_table').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: '<?php echo base_url('user/datatable'); ?>',
				type: 'POST',
			},
			ordering: true,
			searching: true,
			pageLength: 10,
			responsive: true,
			language: {
				search: "<span>Cari Data:</span> _INPUT_",
				searchPlaceholder: 'Ketik Untuk Mencari...',
				loadingRecords: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>',
				processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>',
				lengthMenu: "Menampilkan _MENU_ records per halaman",
				zeroRecords: "Maaf data tidak ditemukan",
				info: "Menampilkan halaman _PAGE_ dari _PAGES_",
				infoEmpty: "Tidak ada data tersedia",
			},
			order: [],
			columns: [{
					data: "no",
					bSortable: false
				},
				{
					data: "nama"
				},
				{
					data: "username"
				},
				{
					mData: null,
					bSortable: false,
					mRender: function(data) {
						var html = '<i class="fas fa-envelope"></i> : ' + data.email;
						html += '<br />';
						html += '<a><i class="fas fa-phone"></i></a> : ';

						if (data.no_hp == undefined) {
							html += '-';
						} else {
							html += data.no_hp;
						}

						return html;
					}
				},
				{
					data: "role"
				},
				{
					data: "is_active"
				},
				{
					mData: null,
					bSortable: false,
					mRender: function(data) {
						return '<button type="button" data-url="<?= site_url('user/view/') ?>' + data.id + '" class="btn btn-info btn-sm detail" data-toggle="modal" data-target="#detailModal"><i class="fas fa-eye"></i></button> <a href="<?php echo base_url('user/update/'); ?>' + data.id + '"><button type="button" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></button></a> <button type="button" data-id="' + data.id + '" data-url="<?= site_url('user/delete/') ?>' + data.id + '" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i></button>';
					}
				},
			],
			columnDefs: [{
					targets: [4],
					render: function(data) {
						if (data == undefined) {
							return '-';
						} else {
							return data;
						}
					}
				},
				{
					targets: [5],
					render: function(data) {
						if (data == '1') {
							return '<span class="badge badge-success"><i class="fa fa-check"></i> Aktif</span>';
						} else {
							return '<span class="badge badge-danger"><i class="fa fa-times"></i> Tidak Aktif</span>';
						}
					}
				},
			],
		});

		$('.select2-dropdown').select2({
			placeholder: "-- Pilih Data --",
			allowClear: true
		});

		$(document).on('click', '.detail', function() {

			$.ajax({
				url: $(this).data('url'),
				type: 'GET',
				beforeSend: function() {
					$('#detailModal .modal-body').html('<h4 class="text-center">Memuat . . .</h4>')
				},
				success: function(response) {
					if (response) {
						$('#detailModal .modal-body').html(response)
					}
				},
				error: function(data) {
					console.log(data)
				}
			})
		})

		$(document).on('click', '.delete', function() {
			var url = $(this).data('url')
			var user_id = $(this).data('id')
			var sess_id = '<?= $this->session->userdata('id') ?>';
			Swal.fire({
				title: 'Konfirmasi Hapus',
				text: "Anda yakin ingin hapus data ini ?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya',
				cancelButtonText: 'Batal',
			}).then(function(result) {
				if (user_id == sess_id) {
					Swal.fire({
						title: 'Gagal!',
						text: 'User Sedang Aktif!',
						icon: 'error',
					})
				} else {
					if (result.value) {
						Swal.fire({
							title: 'Sukses!',
							text: 'Data Berhasil Dihapus!',
							icon: 'success',
							showConfirmButton: false,
							timer: 1500
						}).then(function(result) {
							$.ajax({
								url: url,
								type: 'DELETE',
								success: function(response) {
									$('#user_table').DataTable().ajax.reload()
								},
							})
						})
					}
				}
			})
		})
	});
</script>

<section class="section">
	<div class="section-header">
		<h1>Users</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
			<div class="breadcrumb-item"><a href="#">User</a></div>
			<div class="breadcrumb-item">Update</div>
		</div>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="update-tab" data-toggle="tab" href="#update" role="tab" aria-controls="update" aria-selected="true">
									<h4>Update User</h4>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="false">
									<h4>Ubah Password</h4>
								</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="update" role="tabpanel" aria-labelledby="update-tab">
								<?php $this->load->view('user/_form-update') ?>
							</div>
							<div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
								<?php $this->load->view('user/_form-ubah-password') ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

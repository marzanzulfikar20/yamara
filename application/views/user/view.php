<table class="table table-striped">
	<tr>
		<th>Nama User</th>
		<td><?= $user->nama ?></td>
	</tr>
	<tr>
		<th>Email</th>
		<td><?= $user->email ?></td>
	</tr>
	<tr>
		<th>No HP</th>
		<td><?= $user->no_hp ?></td>
	</tr>
	<tr>
		<th>Username</th>
		<td><?= $user->username ?></td>
	</tr>
	<tr>
		<th>Role</th>
		<td><?= $user->role ?></td>
	</tr>
	<tr>
		<th>Is Active</th>
		<td><?= ($user->is_active == 1 ? '<span class="badge badge-pill badge-success"><i class="fa fa-check"></i> Aktif</span>' : '<span class="badge badge-pill badge-danger"><i class="fa fa-times"></i> Tidak Aktif</span>') ?></td>
	</tr>
</table>

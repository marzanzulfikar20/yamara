<form method="POST" action="<?= site_url('user/ubahpassword/' . $user->id) ?>">
	<div class="row">
		<div class="col-sm-6 col-md-6">
			<div class="form-group">
				<label for="pass_baru">Password Baru</label>
				<input type="password" name="pass_baru" id="pass_baru" class="form-control" placeholder="Isi Password Baru">
				<span class="text-danger"><?= $this->session->flashdata('pass_baru'); ?></span>
			</div>
		</div>
		<div class="col-sm-6 col-md-6">
			<div class="form-group">
				<label for="pass_baru_confirm">Password Baru Confirm</label>
				<input type="password" name="pass_baru_confirm" id="pass_baru_confirm" class="form-control" placeholder="Isi Password Baru Confirm">
				<span class="text-danger"><?= $this->session->flashdata('pass_baru_confirm'); ?></span>
			</div>
		</div>
		<div class="col-sm-12 col-md-12">
			<div class="form-group">
				<button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>
				<a href="<?= site_url('user') ?>" class="btn btn-danger mr-3 float-right"><i class="fas fa-undo-alt"></i> Kembali</a>
			</div>
		</div>
	</div>
</form>

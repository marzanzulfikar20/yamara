<?php
class Migration_alumni extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'nim' => [
                'type' => 'VARCHAR',
                'constraint' => 8,
                'unique'=>true,
            ],
            'nama' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'tanggal_lahir' => [
                'type' => 'DATE',
            ],
            'tanggal_lulus' => [
                'type' => 'DATE',
            ],
            'ipk' => [
                'type' => 'DECIMAL',
                'decimals' => 2
            ],
            'alamat' => [
                'type' => 'text',
            ],
            'id_provinsi' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'id_kota' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'id_kecamatan' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'id_desa' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'kode_pos' => [
                'type' => 'INT',
                'constraint' => 5
            ],
            'no_hp' => [
                'type' => 'VARCHAR',
                'constraint' => 15
            ],
            'no_telp' => [
                'type' => 'VARCHAR',
                'constraint' => 15,
                'default' => null,
            ],
            'jenis_kelamin' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('alumni');
    }

    public function down()
    {
        $this->dbforge->drop_table('alumni');
    }
}

<?php
class Migration_user extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'image' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'role_id' => [
                'type' => 'int',
                'constraint' => 11,
            ],
            'is_active' => [
                'type' => 'int',
                'constraint' => 1,
            ],
            'date_created' => [
                'type' => 'int',
                'default' => NULL,
            ],
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('user');
    }

    public function down() {
        $this->dbforge->drop_table('user');
    }

}
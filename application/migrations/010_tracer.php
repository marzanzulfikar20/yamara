<?php
class Migration_tracer extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'urutan_perkerjaan' => [
                'type' => 'int',
                'constraint' => 11,
            ],
            'skala_perusahaan' => [
                'type' => 'int',
                'constraint' => 1,
            ],
            'nama_perusahaan' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'posisi' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'bagian' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'gaji' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'masa_tunggu' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'alamat_perusahaan' => [
                'type' => 'TEXT',
            ],
            'kota_perusahaan' => [
                'type' => 'TEXT',
            ],
            'kode_pos' => [
                'type' => 'TEXT',
            ],
            'no_telpon' => [
                'type' => 'TEXT',
            ],
            'fak' => [
                'type' => 'TEXT',
            ],
            'website' => [
                'type' => 'TEXT',
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('tracer');
    }

    public function down()
    {
        $this->dbforge->drop_table('tracer');
    }
}

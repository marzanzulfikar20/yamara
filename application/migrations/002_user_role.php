<?php
class Migration_user_role extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'role' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('user_role');
    }

    public function down() {
        $this->dbforge->drop_table('user_role');
    }

}
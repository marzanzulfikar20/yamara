<?php
class Migration_lowongan_pekerjaan extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'id_industri' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'judul_lowongan' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'lowongan_jabatan' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'tgl_buka' => [
                'type' => 'DATE',
            ],
            'tgl_tutup' => [
                'type' => 'DATE',
            ],
            'deskripsi' => [
                'type' => 'TEXT',
            ],
            'persyaratan' => [
                'type' => 'TEXT',
            ],
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('lowongan_pekerjaan');
    }

    public function down()
    {
        $this->dbforge->drop_table('lowongan_pekerjaan');
    }
}

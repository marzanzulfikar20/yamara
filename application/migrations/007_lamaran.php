<?php
class Migration_lamaran extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'id_lowongan' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'nim' => [
                'type' => 'VARCHAR',
                'constraint' => 8,
            ],
            'status' => [
                'type' => 'INT',
                'constraint' => 1,
            ],
            'berkas_lamaran' => [
                'type' => 'TEXT',
            ],
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('lamaran');
    }

    public function down()
    {
        $this->dbforge->drop_table('lamaran');
    }
}

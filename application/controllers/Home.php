<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('user_model');
		$this->load->model('model_transaksi');
		$this->load->model('model_laporan');
	}

	public function index()
	{
		$user = $this->user_model->count();
		$user_aktif = $this->user_model->count_active();
		$penjualan = $this->model_transaksi->pendapatan_harian();
		$barang_terlaris_perbulan = $this->model_transaksi->barang_terlaris_perbulan();
		$barang_tidak_laku_perbulan = $this->model_transaksi->barang_tidak_laku_perbulan();
		$grafik = $this->model_laporan->grafik();
		$dataGrafik = $this->kelolaGrafik($grafik);
		// echo '<pre>';
		// print_r($barang_tidak_laku_perbulan);
		// exit;

		$data = [
			'total_user' => $user,
			'total' => $user_aktif,
			'penjualan' => $penjualan,
			'barang_terlaris_perbulan' => $barang_terlaris_perbulan,
			'barang_tidak_laku_perbulan' => $barang_tidak_laku_perbulan,
			'grafik_perbulan' => json_encode($dataGrafik)
		];

		$this->template->set('title', 'Dashboard');
		$this->template->set('js', 'dashboard/js');
		$this->template->load('app', 'content', 'dashboard/home', $data);
	}

	public function kelolaGrafik($data)
	{
		$bulan = [
			1 => 'jan',
			2 => 'feb',
			3 => 'mar',
			4 => 'apr',
			5 => 'mei',
			6 => 'jun',
			7 => 'jul',
			8 => 'agu',
			9 => 'sep',
			10 => 'okt',
			11 => 'nov',
			12 => 'des',
		];
		$dataFix = [];
		foreach ($data as $key => $value) {
			$dataFix[$value['bulan']] = $value['jml'];
		}

		// echo '<pre>';
		// print_r($dataFix);
		// exit;

		foreach ($bulan as $key => $value) {
			if (isset($dataFix[$key])) {
				$grafik[$value] = $dataFix[$key];
			} else {
				$grafik[$value] = 0;
			}
		}

		$grafikFix = [];
		$i = 0;
		foreach ($grafik as $key => $value) {
			$grafikFix[$i]['label'] = $key;
			$grafikFix[$i]['data'] = $value;
			$grafikFix[$i]['color'] = $this->randColor();
			$i++;
		}
		return $grafikFix;
	}

	function randColor()
	{
		return 'rgba(' . rand(0, 255) . ',' . rand(0, 255) . ',' . rand(0, 255) . ',' . rand(0, 255) . ')';
	}
}

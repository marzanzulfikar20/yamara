<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_cart extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('model_fc');
        $this->load->model('model_produk');
    }

    function add_to_cart()
    { //fungsi Add To Cart
        $stok_sekarang = $this->db->query("SELECT (SUM(a.jumlah_barang) - SUM(b.jumlah_barang_transaksi)) as stok, SUM(a.jumlah_barang) as pembelian, SUM(b.jumlah_barang_transaksi) as penjualan FROM pembelian as a JOIN detail_transaksi as b ON a.id_barang=b.id_barang WHERE a.id_barang='" . $this->input->post('produk_id') . "'")->row();

        $data = array(
            'id' => $this->input->post('produk_id'),
            'name' => $this->input->post('produk_nama'),
            'price' => $this->input->post('produk_harga'),
            'qty' => $this->input->post('quantity'),
        );

        if (empty($this->cart->contents())) {
            $this->cart->insert($data);
            echo $this->show_cart(); //tampilkan cart setelah added
        } else {
            $cek = true;
            foreach ($this->cart->contents() as $items) {
                if ($items['id'] == $this->input->post('produk_id')) {
                    if ($items['qty'] >= $stok_sekarang->stok) {
                        echo 'lebih_stok';
                    } else {
                        $this->cart->insert($data);
                        echo $this->show_cart(); //tampilkan cart setelah added
                    }
                    $cek = false;
                }
            }
            if ($cek == true) {
                $this->cart->insert($data);
                echo $this->show_cart();
            }
        }
    }

    function show_cart()
    { //Fungsi untuk menampilkan Cart
        $output = '';
        $no = 0;
        foreach ($this->cart->contents() as $items) {
            $no++;
            $output .= '
				<tr>
					<td>' . $items['name'] . '</td>
					<td>' . number_format($items['price']) . '</td>
					<td>' . $items['qty'] . '</td>
					<td>' . number_format($items['subtotal']) . '</td>
					<td><button type="button" id="' . $items['rowid'] . '" class="hapus_cart btn btn-danger btn-xs"><i class="fas fa-trash"></i></button></td>
				</tr>
			';
        }
        $output .= '
			<tr>
				<th colspan="3">Total</th>
				<th colspan="2">' . 'Rp <span id="total_bayar">' . number_format($this->cart->total()) . '</span></th>
			</tr>
            <tr>
				<th colspan="3">Bayar</th>
				<th colspan="2">' . '<input type="number" id="bayar">' . '</th>
			</tr>
            <tr>
				<th colspan="3">Kembalian</th>
				<th colspan="2">' . '<input type="number" id="kembalian" readonly>' . '</th>
			</tr>
		';
        return $output;
    }

    function load_cart()
    { //load data cart
        echo $this->show_cart();
    }

    function hapus_cart()
    { //fungsi untuk menghapus item cart
        $data = array(
            'rowid' => $this->input->post('row_id'),
            'qty' => 0,
        );
        $this->cart->update($data);
        echo $this->show_cart();
    }

    function clear_cart()
    {
        $this->cart->destroy();
        echo $this->show_cart();
    }

    function search_barang()
    {
        $barang = $this->input->post('barang');
        $id_kategori = $this->input->post('id_kategori');
        $this->db->like('nama_barang', $barang);
        // $query = $this->db->get('produk')->result();
        $this->db->select('produk.*, sum(jumlah_barang) as stok');
        $this->db->from('produk');
        $this->db->join('pembelian', 'pembelian.id_barang = produk.id_barang', 'left');
        $this->db->where('produk.id_kategori', $id_kategori);
        $this->db->group_by('produk.id_barang');
        $this->db->having('sum(jumlah_barang) != 0');
        // $this->db->where('stok !=', '0');
        $query = $this->db->get()->result();

        foreach ($query as $key => $row) {
            echo '<div class="col-md-6">
            <div class="thumbnail">
                <img width="200" src="' . base_url() . 'uploads/' . $row->produk_img . '">
                <div class="caption">
                    <p>' . $row->nama_barang . '</p>
                    <div class="row">
                        <div class="col-md-7">
                            <p>Rp ' . number_format($row->harga_barang) . '</p>
                        </div>
                        <div class="col-md-5">
                            <input type="number" name="quantity" id="' . $row->id_barang . '" value="1" max="' . $row->stok . '" class="quantity form-control">
                        </div>
                    </div>
                    <button class="add_cart btn btn-success btn-block" data-produkid="' . $row->id_barang . '" data-produknama="' . $row->nama_barang . '" data-produkharga="' . $row->harga_barang . '" data-stok="' . $row->stok . '" >Add To Cart</button>
                </div>
            </div>
        </div>';
        }
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_laporan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('model_laporan');
		$this->load->model('model_produk');
		$this->load->model('model_transaksi');
	}

	public function index()
	{

		$this->template->set('title', 'Data laporan');
		$this->template->set('js', 'laporan/js');
		$this->template->load('app', 'content', 'laporan/index');
	}

	public function create()
	{

		$produk = $this->model_produk->get_all();

		$this->form_validation->set_rules('id_barang', 'Nama Produk', 'required|trim');
		$this->form_validation->set_rules('harga_beli', 'Harga Beli', 'required|trim');
		$this->form_validation->set_rules('jumlah_barang', 'Jumlah Barang', 'required|trim');
		$this->form_validation->set_rules('tgl', 'Tanggal Beli', 'required|trim');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$this->template->set('title', 'Create laporan');
			$this->template->set('js', 'laporan/js');
			$this->template->load('app', 'content', 'laporan/create_laporan');
		} else {
			$data = [
				'id_barang' => $this->input->post('id_barang'),
				'harga_beli' => $this->input->post('harga_beli'),
				'jumlah_barang' => $this->input->post('jumlah_barang'),
				'tgl' => $this->input->post('tgl'),
				'id_user' => $this->session->userdata('id'),
			];

			$res = $this->model_laporan->insert($data);

			// print_r($this->db->error());
			// exit;
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_laporan');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_laporan');
			}
		}
	}

	public function view($id)
	{
		$data['laporan'] = $this->model_laporan->get_by_id($id);

		$this->load->view('laporan/view', $data);
	}

	public function update($id)
	{
		$laporan = $this->model_laporan->get_by_id($id);

		$this->form_validation->set_rules('harga_beli', 'Harga Beli', 'required|trim');
		$this->form_validation->set_rules('jumlah_barang', 'Jumlah Barang', 'required|trim');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$data = [
				'laporan' => $laporan,
				'id' => $id,
			];

			$this->template->set('title', 'Update laporan');
			$this->template->set('js', 'laporan/js');
			$this->template->load('app', 'content', 'laporan/update', $data);
		} else {

			$data = [
				'harga_beli' => $this->input->post('harga_beli'),
				'jumlah_barang' => $this->input->post('jumlah_barang'),
			];

			$res = $this->model_laporan->update($id, $data);

			// print_r($this->db->error());
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_laporan');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_laporan');
			}
		}
	}

	public function delete($id)
	{
		$this->model_laporan->delete($id);
	}

	public function get_datatable()
	{
		$this->load->library('datatables');
		$this->datatables->select('a.id_pembelian, b.nama_barang, a.harga_beli, a.jumlah_barang, c.nama, a.tgl');
		$this->datatables->from('pembelian as a');
		$this->datatables->join('produk as b', 'a.id_barang = b.id_barang');
		$this->datatables->join('user as c', 'c.id=a.id_user');
		$this->datatables->generate();
	}
	public function laporan()
	{
		if (!isset($_POST['bulan'])) {
			$data = [
				'laporan' => $this->model_transaksi->get_all_pertahun($_POST['tahun']),
				'model_transaksi' => $this->model_transaksi,
				'tahun' => $_POST['tahun']
			];

			$this->template->set('title', 'Laporan Per Tahun');
			$this->template->set('js', 'laporan/js');
			$this->template->load('app', 'content', 'laporan/laporan_pertahun', $data);
		} else {
			$bulan_konversi = [
				1 => 'Januari',
				2 => 'Februari',
				3 => 'Maret',
				4 => 'April',
				5 => 'Mei',
				6 => 'Juni',
				7 => 'Juli',
				8 => 'Agustus',
				9 => 'September',
				10 => 'Oktober',
				11 => 'November',
				12 => 'Desember'
			];
			$data = [
				'laporan' => $this->model_transaksi->get_all_perbulan($_POST['bulan'], $_POST['tahun']),
				'model_transaksi' => $this->model_transaksi,
				'tahun' => $_POST['tahun'],
				'bulan' => $bulan_konversi[$_POST['bulan']]
			];

			$this->template->set('title', 'Laporan Per Bulan');
			$this->template->set('js', 'laporan/js');
			$this->template->load('app', 'content', 'laporan/laporan_perbulan', $data);
		}
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_riwayat extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		is_logged_in();
	}

	public function index()
	{
		$this->template->set('title', 'Data Riwayat Transaksi');
		$this->template->set('js', 'riwayat/js');
		$this->template->load('app', 'content', 'riwayat/riwayat');
	}

	public function view($id)
	{
		// function untuk menampilkan invoice
		$this->db->select('produk.nama_barang, detail_transaksi.id_barang, detail_transaksi.harga_barang, detail_transaksi.jumlah_barang_transaksi');
		$this->db->join('produk', 'detail_transaksi.id_barang=produk.id_barang');
		$invoice = $this->db->get_where('detail_transaksi', ['id_transaksi' => $id])->result();

		$this->db->join('user', 'transaksi.id_user=user.id');
		$transaksi = $this->db->get_where('transaksi', ['id_transaksi' => $id])->row();
		// print_r($transaksi);
		// exit;
		$data = [
			'invoice' => $invoice,
			'id_transaksi' => $id,
			'transaksi' => $transaksi,
		];

		$this->template->set('title', 'Detail Riwayat');
		$this->template->set('js', 'riwayat/js');
		$this->template->load('app', 'content', 'riwayat/detail', $data);
	}

	public function get_datatable()
	{
		$this->load->library('datatables');
		$this->datatables->select('a.id_transaksi, a.id_user, a.tgl, b.nama');
		$this->datatables->from('transaksi as a');
		$this->datatables->join('user as b', 'a.id_user = b.id');
		$this->datatables->order_by('tgl', 'desc');
		$this->datatables->generate();
	}
}

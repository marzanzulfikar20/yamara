<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_produk extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('model_produk');
		$this->load->model('model_kategori');
	}

	public function index()
	{
		$this->template->set('title', 'Data Produk');
		$this->template->set('js', 'produk/js');
		$this->template->load('app', 'content', 'produk/data_produk');
	}

	public function create()
	{
		$kategori = $this->model_kategori->get_all();

		$this->form_validation->set_rules('nama_barang', 'Nama Produk', 'required|trim');
		// $this->form_validation->set_rules('kode_barang', 'Kode Produk', 'required|trim');
		$this->form_validation->set_rules('harga_barang', 'Harga', 'required|trim');
		$this->form_validation->set_rules('kode_barang', 'Kode Produk', 'required|trim|is_unique[produk.kode_barang]', ([
			'is_unique' => 'Kode barang ini telah digunakan! silahkan menggunakan kode barang lain.',
		]));

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$data = ['kategori' => $kategori];
			$this->template->set('title', 'Create Produk');
			$this->template->set('js', 'produk/js');
			$this->template->load('app', 'content', 'produk/create_produk', $data);
		} else {
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 2048;
			$config['encrypt_name']         = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('produk_img')) {
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error', $error['error']);
				redirect('c_produk');
			} else {
				$upload = $this->upload->data();

				$data = [
					'nama_barang' => $this->input->post('nama_barang'),
					'kode_barang' => $this->input->post('kode_barang'),
					'harga_barang' => $this->input->post('harga_barang'),
					'id_kategori' => $this->input->post('id_kategori'),
					'produk_img' => $upload['file_name'],
				];

				$res = $this->model_produk->insert($data);

				if ($res) {
					$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
					redirect('c_produk');
				} else {
					// echo '<pre>';
					// print_r($this->db->error());
					// exit;

					$this->session->set_flashdata('error', 'Data Gagal Diproses!');
					redirect('c_produk');
				}
			}
		}
	}

	public function view($id)
	{
		$data['produk'] = $this->model_produk->get_by_id($id);
		$data['stock'] = $this->model_produk->get_stock_id($id);

		$this->load->view('produk/view', $data);
	}

	public function update($id)
	{
		$produk = $this->model_produk->get_by_id($id);
		$kategori = $this->model_kategori->get_all();

		$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|trim');
		$this->form_validation->set_rules('kode_barang', 'Kode Barang', 'required|trim');
		$this->form_validation->set_rules('harga_barang', 'Harga', 'required|trim');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$data = [
				'produk' => $produk,
				'kategori' => $kategori
			];

			$this->template->set('title', 'Update Produk');
			$this->template->set('js', 'produk/js');
			$this->template->load('app', 'content', 'produk/update', $data);
		} else {
			if ($kode_barang = $this->input->post('kode_barang') != $produk->kode_barang) {
				$this->db->where('kode_barang', $kode_barang);
				$this->db->get('produk');
				$cek_kode = $this->db->count_all_results();
				if ($cek_kode > 0) {
					$this->session->set_flashdata('error_kode_barang', 'Kode barang ini telah digunakan! silahkan menggunakan kode barang lain.');
					redirect('C_produk/update/' . $id);
				}
			}

			if ($_FILES['produk_img']['size'] == 0) {
				$upload['file_name'] = $produk->produk_img;
			} else {
				$config['upload_path']          = './uploads/';
				$config['allowed_types']        = 'gif|jpg|png';
				$config['max_size']             = 2048;
				$config['encrypt_name']         = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if (!$this->upload->do_upload('produk_img')) {
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error', $error['error']);
					redirect('c_produk');
				} else {
					unlink('./uploads/' . $produk->produk_img);
					$upload = $this->upload->data();
				}
			}

			$data = [
				'nama_barang' => $this->input->post('nama_barang'),
				'kode_barang' => $this->input->post('kode_barang'),
				'harga_barang' => $this->input->post('harga_barang'),
				'id_kategori' => $this->input->post('id_kategori'),
				'produk_img' => $upload['file_name'],
			];

			$res = $this->model_produk->update($id, $data);

			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_produk');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_produk');
			}
		}
	}

	public function delete($id)
	{
		$this->model_produk->delete($id);
	}

	public function get_datatable()
	{
		$this->load->library('datatables');
		$this->datatables->select('produk.id_barang, nama_barang, kode_barang, harga_barang, sum(jumlah_barang) as stok');
		$this->datatables->from('produk');
		$this->datatables->join('pembelian', 'pembelian.id_barang = produk.id_barang', 'left', false);
		$this->datatables->group_by('produk.id_barang');
		$this->datatables->generate();
	}

	public function get_data_produk()
	{
		echo json_encode($this->model_produk->get_all_select($this->input->get('q')));
	}
}

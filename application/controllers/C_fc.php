<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_fc extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('model_fc');
		$this->load->model('model_produk');
		$this->load->model('model_kategori');
	}

	public function index()
	{

		$produk = $this->model_produk->get_all();
		$kategori = $this->model_kategori->get_all();
		$data = ['produk' => $produk, 'kategori' => $kategori];
		$this->template->set('title', 'Create Transaksi');
		$this->template->set('js', 'penjualan/jscart');
		$this->template->load('app', 'content', 'penjualan/create_fc', $data);
	}

	public function view($id)
	{
		$data['fc'] = $this->model_fc->get_by_id($id);

		$this->load->view('fc/view', $data);
	}

	public function update($id)
	{
		$fc = $this->model_fc->get_by_id($id);

		$this->form_validation->set_rules('harga_beli', 'Harga Beli', 'required|trim');
		$this->form_validation->set_rules('jumlah_barang', 'Jumlah Barang', 'required|trim');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$data = [
				'fc' => $fc,
				'id' => $id,
			];

			$this->template->set('title', 'Update fc');
			$this->template->set('js', 'fc/js');
			$this->template->load('app', 'content', 'fc/update', $data);
		} else {

			$data = [
				'harga_beli' => $this->input->post('harga_beli'),
				'jumlah_barang' => $this->input->post('jumlah_barang'),
			];

			$res = $this->model_fc->update($id, $data);

			// print_r($this->db->error());
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_fc');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_fc');
			}
		}
	}

	public function delete($id)
	{
		$this->model_fc->delete($id);
	}

	public function get_datatable()
	{
		$this->load->library('datatables');
		$this->datatables->select('a.id_pembelian, b.nama_barang, a.harga_beli, a.jumlah_barang, c.nama, a.tgl');
		$this->datatables->from('pembelian as a');
		$this->datatables->join('produk as b', 'a.id_barang = b.id_barang');
		$this->datatables->join('user as c', 'c.id=a.id_user');
		$this->datatables->generate();
	}

	public function checkout()
	{
		// function untuk menyimpan data dari keranjang ke dalam database
		$this->db->trans_begin();
		try {
			$transaksi = [
				'id_user' => $this->session->userdata('id'),
				'tgl' => date('Y-m-d H:i:s')
			];
			$this->db->insert('transaksi', $transaksi);
			$id_transaksi = $this->db->insert_id();
			// echo '<pre>';
			// print_r($transaksi);
			// exit;

			$cart = $this->cart->contents();

			foreach ($cart as $key => $value) {
				$cek_stock = $this->stock($value['id'], $value['qty']);
				$detail_transaksi = [
					'id_transaksi' => $id_transaksi,
					'id_barang' => $value['id'],
					'harga_barang' => $value['price'],
					'jumlah_barang_transaksi' => $value['qty']
				];
				// echo '<pre>';
				// print_r($detail_transaksi);
				// exit;
				$this->db->insert('detail_transaksi', $detail_transaksi);
			}

			$this->db->trans_commit();
			$this->cart->destroy();

			$this->session->set_flashdata('sukses', 'Transaksi Berhasil Diproses');
			redirect('C_fc/invoice/' . $id_transaksi);
		} catch (\Throwable $th) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Gagal Memproses Transaksi');
			redirect('C_fc/index');
		}
	}

	public function stock($id_barang, $jumlah_barang)
	{
		$this->db->where('id_barang', $id_barang);
		$stock = $this->db->get('pembelian')->result();

		foreach ($stock as $key => $value) {
			if ($jumlah_barang > 0) {
				if ($value->jumlah_barang != '0') {
					$stock_sekarang = $value->jumlah_barang - $jumlah_barang;
					$data = [
						'jumlah_barang' => $stock_sekarang
					];

					$this->db->where('id_pembelian', $value->id_pembelian);
					$this->db->update('pembelian', $data);
				}
			}
			$jumlah_barang = $jumlah_barang - $value->jumlah_barang;
		}
	}

	public function invoice($id_transaksi)
	{
		// function untuk menampilkan invoice
		$this->db->select('produk.nama_barang, detail_transaksi.id_barang, detail_transaksi.harga_barang, detail_transaksi.jumlah_barang_transaksi');
		$this->db->join('produk', 'detail_transaksi.id_barang=produk.id_barang');
		$invoice = $this->db->get_where('detail_transaksi', ['id_transaksi' => $id_transaksi])->result();
		$data = [
			'invoice' => $invoice,
			'id_transaksi' => $id_transaksi,
		];

		$this->template->set('title', 'Checkout');
		$this->template->set('js', 'penjualan/js');
		$this->template->load('app', 'content', 'penjualan/checkout', $data);
	}

	public function cetak($id_transaksi)
	{
		$this->db->select('produk.nama_barang, detail_transaksi.id_barang, detail_transaksi.harga_barang, detail_transaksi.jumlah_barang_transaksi');
		$this->db->join('produk', 'detail_transaksi.id_barang=produk.id_barang');
		$invoice = $this->db->get_where('detail_transaksi', ['id_transaksi' => $id_transaksi])->result();

		$this->db->join('user', 'transaksi.id_user=user.id');
		$transaksi = $this->db->get_where('transaksi', ['id_transaksi' => $id_transaksi])->row();

		$this->load->view('penjualan/invoice', [
			'invoice' => $invoice, 'transaksi' => $transaksi
		]);
	}
}

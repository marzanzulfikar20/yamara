<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		can(['admin']);
		$this->load->model('user_model');
	}

	public function index()
	{
		$user = $this->user_model->get_all();

		$data = [
			'user' => $user,
		];

		$this->template->set('title', 'User');
		$this->template->set('js', 'user/js');
		$this->template->load('app', 'content', 'user/index', $data);
	}

	public function create()
	{

		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', ([
			'is_unique' => 'Email ini telah digunakan! silahkan menggunakan email lain.',
		]));
		$this->form_validation->set_rules('no_hp', 'No HP', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[user.username]', ([
			'is_unique' => 'Username ini telah digunakan! silahkan menggunakan username lain.',
		]));
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[8]', ([
			'min_length' => 'password terlalu pendek! minimal 8 karakter',
		]));
		$this->form_validation->set_rules('role', 'Role', 'required|trim');
		$this->form_validation->set_rules('is_active', 'Is Active', 'required|trim');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			

			$this->template->set('title', 'Create User');
			$this->template->set('js', 'user/js');
			$this->template->load('app', 'content', 'user/create');
		} else {
			$data = [
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'no_hp' => $this->input->post('no_hp'),
				'username' => $this->input->post('username'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'role' => $this->input->post('role'),
				'is_active' => $this->input->post('is_active'),
			];

			$res = $this->user_model->insert($data);
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('user');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('user');
			}
		}
	}

	public function update($id)
	{
		$user = $this->user_model->get_by_id($id);

		$this->form_validation->set_rules('nama', 'Name\a', 'required|trim');
		$this->form_validation->set_rules('no_hp', 'No HP', 'required|trim');
		$this->form_validation->set_rules('role', 'Role', 'required|trim');
		$this->form_validation->set_rules('is_active', 'Is Active', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$data = [
				'user' => $user,
			];

			$this->template->set('title', 'Update User');
			$this->template->set('js', 'user/js');
			$this->template->load('app', 'content', 'user/update', $data);
		} else {
			if ($username = $this->input->post('username') != $user->username) {
				$this->db->where('username', $username);
				$this->db->get('user');
				$check_username = $this->db->count_all_results();
				if ($check_username > 0) {
					$this->session->set_flashdata('error_username', 'Username ini telah digunakan! silahkan menggunakan username lain.');
					redirect('user/update/' . $id);
				}
			}

			if ($email = $this->input->post('email') != $user->email) {
				$this->db->where('email', $email);
				$this->db->get('user');
				$check_email = $this->db->count_all_results();
				if ($check_email > 0) {
					$this->session->set_flashdata('error_email', 'Email ini telah digunakan! silahkan menggunakan email lain.');
					redirect('user/update/' . $id);
				}
			}

			$data = [
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'no_hp' => $this->input->post('no_hp'),
				'username' => $this->input->post('username'),
				'role' => $this->input->post('role'),
				'is_active' => $this->input->post('is_active'),

			];

			$res = $this->user_model->update($id, $data);
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('user');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('user');
			}
		}
	}

	public function view($id)
	{
		$data['user'] = $this->user_model->get_by_id($id);

		$this->load->view('user/view', $data);
	}

	public function delete($id)
	{
		$this->user_model->delete($id);
	}

	public function ubah_password($id)
	{
		$pass_baru = $this->input->post('pass_baru');

		$this->form_validation->set_rules('pass_baru', 'Password Baru', 'required|trim|min_length[8]|matches[pass_baru_confirm]', ([
			'matches' => 'Password Tidak Sama!',
			'min_length' => 'Password Terlalu Pendek!',
		]));
		$this->form_validation->set_rules('pass_baru_confirm', 'Password Baru Confirm', 'required|trim|min_length[8]|matches[pass_baru]', ([
			'matches' => 'Password Tidak Sama!',
			'min_length' => 'Password Terlalu Pendek!',
		]));

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$this->session->set_flashdata('pass_baru', form_error('pass_baru'));
			$this->session->set_flashdata('pass_baru_confirm', form_error('pass_baru_confirm'));

			redirect('user/update/' . $id);
		} else {
			$data = [
				'password' => password_hash($pass_baru, PASSWORD_DEFAULT),
			];

			$this->user_model->update($id, $data);
			$this->session->set_flashdata('sukses', 'Update Password Berhasil!');
			redirect('user');
		}
	}

	public function get_datatable()
	{
		$this->load->library('datatables');
		$this->datatables->select('a.id as id, nama, username, email, no_hp, role, is_active');
		$this->datatables->from('user as a');
		$this->datatables->generate();
	}
}

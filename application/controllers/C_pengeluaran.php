<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_pengeluaran extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('model_pengeluaran');
	}

	public function index()
	{
		
		$this->template->set('title', 'Data Pengeluaran');
		$this->template->set('js', 'pengeluaran/js');
		$this->template->load('app', 'content', 'pengeluaran/index');
	}

		public function create()
	{
		
		$produk = $this->model_pengeluaran->get_all();

		$this->form_validation->set_rules('jenis_pengeluaran', 'Jenis Pengeluaran', 'required|trim');
		$this->form_validation->set_rules('biaya_pengeluaran', 'Biaya Pengeluaran', 'required|trim');
		$this->form_validation->set_rules('tgl', 'Tanggal', 'required|trim');
		
		
		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$this->template->set('title', 'Create Pengeluaran');
			$this->template->set('js', 'pengeluaran/js');
			$this->template->load('app', 'content', 'pengeluaran/create_pengeluaran');
		} else {
			$data = [
				'jenis_pengeluaran' => $this->input->post('jenis_pengeluaran'),
				'biaya_pengeluaran' => $this->input->post('biaya_pengeluaran'),
				'tgl' => $this->input->post('tgl'),
				'id_user' => $this->session->userdata('id'),
			];
			
			$res = $this->model_pengeluaran->insert($data);

			// print_r($this->db->error());
			// exit;
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_pengeluaran');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_pengeluaran');
			}
		}
	}

	public function view($id)
	{
		$data['pengeluaran'] = $this->model_pengeluaran->get_by_id($id);

		$this->load->view('pengeluaran/view', $data);
	}

	public function update($id)
	{
		$pengeluaran = $this->model_pengeluaran->get_by_id($id);

		$this->form_validation->set_rules('jenis_pengeluaran', 'Jenis Pengeluaran', 'required|trim');
		$this->form_validation->set_rules('biaya_pengeluaran', 'Biaya Pengeluaran', 'required|trim');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$data = [
				'pengeluaran' => $pengeluaran,
				'id' => $id,
			];

			$this->template->set('title', 'Update Pengeluaran');
			$this->template->set('js', 'pengeluaran/js');
			$this->template->load('app', 'content', 'pengeluaran/update', $data);
		} else {
			
			$data = [
				'jenis_pengeluaran' => $this->input->post('jenis_pengeluaran'),
				'biaya_pengeluaran' => $this->input->post('biaya_pengeluaran'),
			];

			$res = $this->model_pengeluaran->update($id, $data);

			// print_r($this->db->error());
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_pengeluaran');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_pengeluaran');
			}
		}
	}

	public function delete($id)
	{
		$this->model_pengeluaran->delete($id);
	}

	public function get_datatable()
	{
		$this->load->library('datatables');
		$this->datatables->select('a.id_pengeluaran, a.jenis_pengeluaran, a.biaya_pengeluaran, b.nama, a.tgl');
		$this->datatables->from('pengeluaran as a');
		$this->datatables->join('user as b', 'b.id=a.id_user');
		$this->datatables->generate();
	}
}

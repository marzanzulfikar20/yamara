<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_belanja extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('model_belanja');
		$this->load->model('model_produk');
	}

	public function index()
	{
		
		$this->template->set('title', 'Data Belanja');
		$this->template->set('js', 'belanja/js');
		$this->template->load('app', 'content', 'belanja/index');
	}

		public function create()
	{
		
		$produk = $this->model_produk->get_all();

		$this->form_validation->set_rules('id_barang', 'Nama Produk', 'required|trim');
		$this->form_validation->set_rules('harga_beli', 'Harga Beli', 'required|trim');
		$this->form_validation->set_rules('jumlah_barang', 'Jumlah Barang', 'required|trim');
		$this->form_validation->set_rules('tgl', 'Tanggal Beli', 'required|trim');
		
		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$this->template->set('title', 'Create Belanja');
			$this->template->set('js', 'belanja/js');
			$this->template->load('app', 'content', 'belanja/create_belanja');
		} else {
			$data = [
				'id_barang' => $this->input->post('id_barang'),
				'harga_beli' => $this->input->post('harga_beli'),
				'jumlah_barang' => $this->input->post('jumlah_barang'),
				'tgl' => $this->input->post('tgl'),
				'id_user' => $this->session->userdata('id'),
			];

			$res = $this->model_belanja->insert($data);

			// print_r($this->db->error());
			// exit;
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_belanja');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_belanja');
			}
		}
	}

	public function view($id)
	{
		$data['belanja'] = $this->model_belanja->get_by_id($id);

		$this->load->view('belanja/view', $data);
	}

	public function update($id)
	{
		$belanja = $this->model_belanja->get_by_id($id);

		$this->form_validation->set_rules('harga_beli', 'Harga Beli', 'required|trim');
		$this->form_validation->set_rules('jumlah_barang', 'Jumlah Barang', 'required|trim');
		
		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong.');

		if ($this->form_validation->run() == false) {
			$data = [
				'belanja' => $belanja,
				'id' => $id,
			];

			$this->template->set('title', 'Update Belanja');
			$this->template->set('js', 'belanja/js');
			$this->template->load('app', 'content', 'belanja/update', $data);
		} else {
			
			$data = [
				'harga_beli' => $this->input->post('harga_beli'),
				'jumlah_barang' => $this->input->post('jumlah_barang'),
			];

			$res = $this->model_belanja->update($id, $data);

			// print_r($this->db->error());
			if ($res) {
				$this->session->set_flashdata('sukses', 'Data Berhasil Diproses!');
				redirect('c_belanja');
			} else {
				// echo '<pre>';
				// print_r($this->db->error());
				// exit;

				$this->session->set_flashdata('error', 'Data Gagal Diproses!');
				redirect('c_belanja');
			}
		}
	}

	public function delete($id)
	{
		$this->model_belanja->delete($id);
	}

	public function get_datatable()
	{
		$this->load->library('datatables');
		$this->datatables->select('a.id_pembelian, b.nama_barang, a.harga_beli, a.jumlah_barang, c.nama, a.tgl');
		$this->datatables->from('pembelian as a');
		$this->datatables->join('produk as b', 'a.id_barang = b.id_barang');
		$this->datatables->join('user as c', 'c.id=a.id_user');
		$this->datatables->generate();
	}
}

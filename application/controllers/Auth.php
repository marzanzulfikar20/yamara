<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		has_logged_in();
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->load->view('auth/login');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user = $this->db->get_where('user', ['username' => $username])->row_array();

		if ($user) {
			if ($user['is_active'] == 0) {
				$this->session->set_flashdata('message_error', 'User Tidak Aktif! Silahkan Hubungi Admin.');
				redirect('auth');
			} else {
				if (password_verify($password, $user['password'])) {
					$this->session->set_userdata($user);
					$this->session->set_flashdata('login_sukses', 'Login Sukses!');
					redirect('home');
				} else {
					$this->session->set_flashdata('message_error', 'Password Salah!');
					redirect('auth');
				}
			}
		} else {
			$this->session->set_flashdata('message_error', 'Username Salah/Tidak Ditemukan');
			redirect('auth');
		}
	}

	public function logout()
	{
		foreach ($this->session->all_userdata() as $key => $val) {
			$this->session->unset_userdata($key);
		}

		$this->session->set_flashdata('logout', 'Sukses Logout!');
		redirect('auth');
	}

	public function blocked()
	{
		$this->template->set('title', '403');
		$this->template->load('app', 'content', 'auth/blocked');
	}

	public function my_404()
	{
		$this->template->set('title', '404');
		$this->template->load('app', 'content', 'auth/error_404');
	}
}

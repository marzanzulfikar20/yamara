<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_produk extends CI_Model
{

	public $table = 'produk';
	public $id = 'id_barang';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get all
	function get_all_array()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result_array();
	}

	// get all
	function get_all_select($param)
	{
		// $this->db->order_by($this->id, $this->order);
		$this->db->like('nama_barang', $param);
		return $this->db->get($this->table)->result();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('produk');
		$this->db->where($this->id, $id);
		return $this->db->get()->row();
	}

	function get_stock_id($id)
	{
		$this->db->select('produk.id_barang, nama_barang, kode_barang, harga_barang, sum(jumlah_barang) as stok');
		$this->db->from('produk');
		$this->db->join('pembelian', 'pembelian.id_barang = produk.id_barang', 'left');
		$this->db->where('produk.id_barang', $id);
		return $this->db->get()->result();
	}

	// insert data
	function insert($data)
	{
		return $this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->delete($this->table);
	}

	function count()
	{
		return $this->db->get($this->table)->num_rows();
	}
}

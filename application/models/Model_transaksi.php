<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_transaksi extends CI_Model
{

    public $table = 'transaksi';
    public $id = 'id_transaksi';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function pendapatan_harian()
    {
        $this->db->select('sum(harga_barang*jumlah_barang_transaksi) as pendapatan');
        $this->db->from('transaksi');
        $this->db->join('detail_transaksi', 'detail_transaksi.id_transaksi=transaksi.id_transaksi');
        $this->db->where('date(tgl)', date('Y-m-d'));
        return $this->db->get()->row_array();
    }

    function barang_terlaris_perbulan()
    {
        $this->db->select('c.nama_barang, SUM(a.jumlah_barang_transaksi) as sum');
        $this->db->from('detail_transaksi as a');
        $this->db->join('transaksi as b', 'a.id_transaksi=b.id_transaksi');
        $this->db->join('produk as c', 'a.id_barang=c.id_barang');
        $this->db->where('month(b.tgl)', date('m'));
        $this->db->group_by('a.id_barang');
        $this->db->order_by('sum desc');
        $this->db->limit('5');
        return $this->db->get()->result_array();
    }

    function barang_tidak_laku_perbulan()
    {
        return $this->db->query("SELECT x.nama_barang, y.sum FROM produk as x LEFT JOIN (SELECT `a`.`id_barang`, SUM(a.jumlah_barang_transaksi) as sum FROM `detail_transaksi` as `a`RIGHT JOIN `transaksi` as `b` ON `a`.`id_transaksi`=`b`.`id_transaksi` WHERE month(b.tgl) = '01' GROUP BY `a`.`id_barang` ORDER BY `sum` asc) as y ON x.id_barang = y.id_barang ORDER BY y.sum asc LIMIT 5")->result_array();
    }

    function detail_transaksi_join($id_transaksi)
    {
        $this->db->select('detail_transaksi.*, a.nama_barang');
        $this->db->from('detail_transaksi');
        $this->db->join('produk as a', 'a.id_barang=detail_transaksi.id_barang');
        $this->db->where('detail_transaksi.id_transaksi', $id_transaksi);
        // $this->db->order_by('tgl desc');
        return $this->db->get()->result();
    }

    function get_all_perbulan($bulan, $tahun)
    {
        $this->db->select('transaksi.*, a.nama');
        $this->db->from('transaksi');
        $this->db->join('user as a', 'a.id=transaksi.id_user');
        $this->db->where('month(tgl)', $bulan);
        $this->db->where('year(tgl)', $tahun);
        $this->db->order_by('tgl desc');
        return $this->db->get()->result();
    }

    function get_all_pertahun($tahun)
    {
        $this->db->select('transaksi.*, a.nama');
        $this->db->from('transaksi');
        $this->db->join('user as a', 'a.id=transaksi.id_user');
        $this->db->where('year(tgl)', $tahun);
        $this->db->order_by('tgl desc');
        return $this->db->get()->result();
    }
}

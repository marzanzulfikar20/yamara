<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_belanja extends CI_Model
{

	public $table = 'pembelian';
	public $id = 'id_pembelian';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get all
	function get_all_array()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result_array();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->select('pembelian.*, produk.nama_barang, user.nama');
		$this->db->from('pembelian');
		$this->db->join('produk', 'produk.id_barang=pembelian.id_barang');
		$this->db->join('user', 'user.id=pembelian.id_user');
		$this->db->where($this->id, $id);
		return $this->db->get()->row();
	}

	// insert data
	function insert($data)
	{
		return $this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->delete($this->table);
	}

	function count()
	{
		return $this->db->get($this->table)->num_rows();
	}
}
?>
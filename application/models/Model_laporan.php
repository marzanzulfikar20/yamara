<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_laporan extends CI_Model
{

	// public $table = 'laporan';
	// public $id = 'id_barang';
	// public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get all
	function get_all_array()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result_array();
	}

	// get all
	function get_all_select($param)
	{
		// $this->db->order_by($this->id, $this->order);
		$this->db->like('nama_barang', $param);
		return $this->db->get($this->table)->result();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('produk');
		$this->db->where($this->id, $id);
		return $this->db->get()->row();
	}

	// insert data
	function insert($data)
	{
		return $this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->delete($this->table);
	}

	function count()
	{
		return $this->db->get($this->table)->num_rows();
	}

	function grafik()
	{
		$this->db->select('month(b.tgl) as bulan, sum(a.harga_barang*a.jumlah_barang_transaksi) as jml');
		$this->db->from('detail_transaksi as a');
		$this->db->join('transaksi as b', 'a.id_transaksi=b.id_transaksi');
		$this->db->where('year(b.tgl)', date('Y'));
		$this->db->group_by('month(b.tgl)');
		return $this->db->get()->result_array();
	}
}

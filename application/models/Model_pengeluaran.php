<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_pengeluaran extends CI_Model
{

	public $table = 'pengeluaran';
	public $id = 'id_pengeluaran';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get all
	function get_all_array()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result_array();
	}

	// get all
	function get_all_select($param)
	{
		// $this->db->order_by($this->id, $this->order);
		$this->db->like('nama_barang',$param);
		return $this->db->get($this->table)->result();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('pengeluaran');
		$this->db->where($this->id, $id);
		$this->db->join('user', 'user.id=pengeluaran.id_user');
		return $this->db->get()->row();
	}

	// insert data
	function insert($data)
	{
		return $this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->delete($this->table);
	}

	function count()
	{
		return $this->db->get($this->table)->num_rows();
	}
}
?>
<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{

	public $table = 'user';
	public $id = 'id';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get all
	function get_all_array()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result_array();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->select('a.id as id, nama, username, email, no_hp, role, is_active');
		$this->db->from('user as a');
		$this->db->where('a.id', $id);
		return $this->db->get()->row();
	}

	// insert data
	function insert($data)
	{
		return $this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->delete($this->table);
	}

	function count()
	{
		return $this->db->get($this->table)->num_rows();
	}

	function count_active()
	{
		$this->db->where('is_active', 1);
		return $this->db->get($this->table)->num_rows();
	}
}

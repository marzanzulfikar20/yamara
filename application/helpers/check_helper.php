<?php
defined('BASEPATH') or exit('No direct script access allowed');

function is_logged_in()
{
	$ci = get_instance();
	if (!$ci->session->userdata('username')) {
		$ci->session->set_flashdata('login_first', 'Silahkan Login Terlebih Dahulu!');
		redirect('auth');
	}
}

function can($roles = [])
{
	$ci = get_instance();
	$role = $ci->session->userdata('role');
	if (!in_array($role, $roles)) {
		redirect('auth/blocked');
	}
}

function has_logged_in()
{
	$ci = get_instance();
	if ($ci->session->userdata('username')) {
		$ci->session->set_flashdata('message_warning', 'Anda Sudah Login.');
		redirect('home');
	}
}
